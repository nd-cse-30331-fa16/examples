// traversal.cpp

#include <iostream>
#include <memory>
#include <queue>
#include <stack>
#include <string>
#include <vector>

using namespace std;

// Node structure

template <typename T>
struct Node {
    T		     data;
    shared_ptr<Node> left;
    shared_ptr<Node> right;
};

// Pre-defined Trees

#define UNIQUE_NODE(x, y, z) shared_ptr<Node<char>>(new Node<char>{x, y, z})

Node<char> *AlgorithmTree() {
    return new Node<char>{'A',
    	UNIQUE_NODE('L',
    	    UNIQUE_NODE('O',
		UNIQUE_NODE('H', nullptr, nullptr),
		UNIQUE_NODE('M', nullptr, nullptr)
	    ),
	    UNIQUE_NODE('R', nullptr, nullptr)
	),
	UNIQUE_NODE('G',
	    UNIQUE_NODE('I', nullptr, nullptr),
	    UNIQUE_NODE('T', nullptr, nullptr)
	)
    };
}

Node <char> *WikipediaTree() {
    return new Node<char>{'F',
    	UNIQUE_NODE('B',
	    UNIQUE_NODE('A', nullptr, nullptr),
	    UNIQUE_NODE('D',
		UNIQUE_NODE('C', nullptr, nullptr),
		UNIQUE_NODE('E', nullptr, nullptr)
	    )
	),
	UNIQUE_NODE('G',
	    nullptr,
	    UNIQUE_NODE('I',
		UNIQUE_NODE('H', nullptr, nullptr),
	    	nullptr
	    )
	)
    };
}

Node <char> *Reading02Tree() {
    return new Node<char>{'W',
    	UNIQUE_NODE('Y',
	    UNIQUE_NODE('D',
		UNIQUE_NODE('F', nullptr, nullptr),
		UNIQUE_NODE('U', nullptr, nullptr)
	    ),
	    UNIQUE_NODE('B',
		UNIQUE_NODE('L', nullptr, nullptr),
		UNIQUE_NODE('A', nullptr, nullptr)
	    )
	),
	UNIQUE_NODE('R',
	    UNIQUE_NODE('I',
		UNIQUE_NODE('R', nullptr, nullptr),
		UNIQUE_NODE('A', nullptr, nullptr)
	    ),
	    UNIQUE_NODE('O',
		UNIQUE_NODE('E', nullptr, nullptr),
		UNIQUE_NODE('D', nullptr, nullptr)
	    )
	)
    };
}

// Traversals

template <typename T>
void bfs(shared_ptr<Node<T>> &root) {
    queue<shared_ptr<Node<T>>> q;

    q.push(root);

    while (!q.empty()) {
    	auto n = q.front(); q.pop();
    	cout << n->data << " ";

    	if (n->left)  q.push(n->left);
    	if (n->right) q.push(n->right);
    }

    cout << endl;
}

template <typename T>
void dfs(shared_ptr<Node<T>> &root) {
    stack<shared_ptr<Node<T>>> s;

    s.push(root);

    while (!s.empty()) {
    	auto n = s.top(); s.pop();
    	cout << n->data << " ";

    	if (n->right) s.push(n->right);
    	if (n->left)  s.push(n->left);
    }

    cout << endl;
}

template <typename T>
void dfs_recursive(shared_ptr<Node<T>> &root) {
    if (root == nullptr) {
    	return;
    }

    cout << root->data << " ";

    if (root->left)  dfs_recursive(root->left);
    if (root->right) dfs_recursive(root->right);
}

int main(int argc, char *argv[]) {
    auto tree = shared_ptr<Node<char>>(AlgorithmTree());
    //auto tree = shared_ptr<Node<char>>(WikipediaTree());
    //auto tree = shared_ptr<Node<char>>(Reading02Tree());

    cout << "BFS: "; bfs(tree);
    cout << "DFS: "; dfs(tree);
    cout << "DFS: "; dfs_recursive(tree); cout << endl;

    return 0;
}
