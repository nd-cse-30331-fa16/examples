// compressed_set.hpp

#pragma once

#include <cmath>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

// FNV Hash Function

#define FNV_OFFSET_BASIS    (0xcbf29ce484222325ULL)
#define FNV_PRIME	    (0x100000001b3ULL)

size_t fnv_string_hash(const std::string& s) {
    uint64_t hash = FNV_OFFSET_BASIS;

    for (uint8_t i = 0; i < s.size(); i++) {
	uint8_t byte = static_cast<uint8_t>(s[i]);
	hash = hash ^ byte;
	hash = hash * FNV_PRIME;
    }

    return hash;
}

// Compressed Set Class

class compressed_set {
public:
    compressed_set(int n, double epsilon) : k(-std::log2(epsilon)), t(k*n/std::log(2), 0) {
	std::cout << "Given n = " << n << " and epsilon = " << epsilon << ", we will need " << k << " hash functions and a bitset of size " << t.size() << std::endl;
    }

    bool search(const std::string &s) {
	for (size_t i = 0; i < k; i++)
	    if (!t[hash(s, i) % t.size()])
		return false;
	return true;
    }

    void insert(const std::string &s) {
	for (size_t i = 0; i < k; i++)
	    t[hash(s, i) % t.size()] = true;
    }

private:
    // Composite hash function
    size_t hash(const std::string &s, int i) {
    	auto stl_string_hash = std::hash<std::string>();
    	return stl_string_hash(s) + i*fnv_string_hash(s);
    }
    
    size_t		k; // Number of hash functions
    std::vector<bool>	t; // Bitset
};
