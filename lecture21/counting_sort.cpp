#include <cassert>
#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

#define MIN_VALUE   (0)
#define MAX_VALUE   (1000000)

void counting_sort(vector<int> &v) {
    vector<int> counts(MAX_VALUE + 1);

    // Compute histograms
    for (auto i : v) {
    	counts[i]++;
    }

    // Update input vector with appropriate values
    size_t index = 0;
    for (size_t i = 0; i < counts.size(); i++) {
	for (int c = 0; c < counts[i]; c++) {
	    v[index++] = i;
	}
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return EXIT_FAILURE;
    }
    
    size_t nitems = atoi(argv[1]);
    vector<int> v(nitems);

    default_random_engine generator;
    uniform_int_distribution<int> distribution(MIN_VALUE, MAX_VALUE);

    for (size_t i = 0; i < nitems; i++) {
    	v[i] = distribution(generator);
    }
    
    counting_sort(v);

    for (size_t i = 1; i < nitems; i++) {
    	assert(v[i] >= v[i - 1]);
    }

    return 0;
}
